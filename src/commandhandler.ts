import { ICommandHandler, ICommandHandlerArgRestriction, ICommandHandlerData } from "./interfaces/icommandhandler";

export abstract class CommandHandler implements ICommandHandler {
    protected abstract command: string;
    protected abstract argRestrictions: ICommandHandlerArgRestriction[];

    getCommand(): string {
        return this.command;
    }
    getArgRestrictions(): ICommandHandlerArgRestriction[] {
        return this.argRestrictions;
    }
    abstract handleCommand(data: ICommandHandlerData): void;
    getHelp(): string {
        const argHelps: string[] = this.argRestrictions.map(ar => ar.getHelp());
        return `Usage: ${this.command} ${argHelps.reduce((prev, curr) => prev + " " + curr), ""}`;
    }

}