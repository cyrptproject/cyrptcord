import { ICommandHandlerArgRestriction, ICommandHandlerArgType } from "./interfaces/icommandhandler";

export class CommandHandlerArgRestriction implements ICommandHandlerArgRestriction {
    position: number;
    argType: ICommandHandlerArgType;
    name: string

    constructor(name: string, position: number, argType: ICommandHandlerArgType) {
        this.position = position;
        this.argType = argType;
        this.name = name;
    }

    applyRestriction(content: string): any {
        let value: any = null;
        switch(this.argType) {
            case ICommandHandlerArgType.ANY: value = content;
            case ICommandHandlerArgType.NUMBER: value = Number(content);
            case ICommandHandlerArgType.INTEGER: value = parseInt(content);
            case ICommandHandlerArgType.FLOAT: value = parseFloat(content);
        }
        return value;
    }

    getHelp(): string {
        return `${this.name}`;
    }

}