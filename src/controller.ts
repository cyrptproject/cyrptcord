import {Client, Message} from 'discord.js';
import { ICommandHandler, ICommandHandlerArgType } from './interfaces/icommandhandler';
import { IModule } from './interfaces/imodule';
import * as fs from 'fs';
import { IConfig } from './interfaces/iconfig';
import { getContextFreeDirectory } from './utility';
import {PersistentStorageHandler} from './storage';

export class Controller {
    private client: Client;
    private config: IConfig;
    private commandHandlers: Map<string, ICommandHandler>;
    private modules: Map<string, IModule>;
    private modulePaths: Map<string, string>;
    private storage: PersistentStorageHandler;

    private MODULE_DIRECTORY: string = 'modules';
    private DEFAULT_CONFIG_DIR = '../config.json';

    constructor() {
        this.config = require(process.env.CONFIG_LOCATION || this.DEFAULT_CONFIG_DIR);
        this.client = new Client();
        this.commandHandlers = new Map<string, ICommandHandler>();
        this.modules = new Map<string, IModule>()
        this.storage = new PersistentStorageHandler();
        this.modulePaths = new Map<string, string>();
    }

    private getDiscordAPIToken(config: {token: string}): string {
        return process.env.DISCORD_TOKEN || config.token;
    }
    
    private clientLogin(): Promise<Error | null> {
        return new Promise((resolve, reject) => {
            const loginFailed = (err: Error) => {console.error("An error occurred while logging in."); reject(err);}
            const loginSucceeded = () => {
                this.client.off('error', loginFailed);
                resolve(null);
            }
            this.client.login(this.getDiscordAPIToken(this.config)).catch(loginFailed).then(loginSucceeded);
        })
    }

    private messageHandler(message: Message) {
        if(!message.content.startsWith(this.config.prefix) || message.author.bot) {
            return;
        }
        const commandComponents: string[] = message.content.substr(this.config.prefix.length).split(' ');
        const command: string = commandComponents[0];
        const commandHandler : ICommandHandler | undefined = this.commandHandlers.get(command);
        if(!commandHandler) {
            return;
        }
        console.log(`Receieved command ${command} from ${message.author} in channel ${message.channel} on server ${message.guild}`);
        const args: string[] = commandComponents.slice(1);
        const convertedArgs: any[] = commandHandler.getArgRestrictions().map(argRestriction => argRestriction.applyRestriction(args[argRestriction.position]));
        if(convertedArgs.filter(arg => arg == null).length > 0 || convertedArgs.length < commandHandler.getArgRestrictions().length) {
            message.channel.send(commandHandler.getHelp());
            return;
        }
        commandHandler.handleCommand({message, args: convertedArgs});
    }

    //TODO: add config -> env var mapping json to automatically pull from env variables if they exist and overwrite in memory config
    private parseConfig(config: IConfig) {
        
    }

    private initializeClientListeners() {
        this.client.on('message', message => this.messageHandler(message));
        this.client.on('disconnect', () => console.log("Bot disconnected"));
    }

    private loadModules() {
        const MODULES_DIR = getContextFreeDirectory(this.MODULE_DIRECTORY);
        const moduleDirs = fs.readdirSync(MODULES_DIR, {withFileTypes: true})
        .filter(fileName => fileName.isDirectory())
        .map(dir => dir.name);
        this.modules = moduleDirs.filter(dir => fs.existsSync(`${MODULES_DIR}/${dir}/module_descriptor.js`))
        .reduce<Map<string, IModule>>((mapping, dir) => {
            const MODULE_DIR: string = `${MODULES_DIR}/${dir}`;
            const mod: IModule = require(`${MODULE_DIR}/module_descriptor.js`);
            mapping.set(mod.identifier, mod);
            this.modulePaths.set(mod.identifier, MODULE_DIR);
            console.log(`Loading module ${mod.name} with identifier ${mod.identifier}.`);
            return mapping;
        }, new Map<string, IModule>());
    }

    private preInit() {
        this.modules.forEach(module => module.preInit({storage: this.storage, absoluteModulePath: this.modulePaths.get(module.identifier)}));
    }

    private init() {
        this.modules.forEach(module => module.init({controller: this}));
    }

    private postInit() {
        this.modules.forEach(module => module.postInit());
    }

    public async start() {
        try {
            await this.clientLogin();
            this.loadModules();
            this.preInit();
            this.init();
            this.postInit();
            this.initializeClientListeners();
        }
        catch(e) {
            console.error("Error in bot startup");
            console.log(e);
        }

    }

    public registerCommandHandler(commandHandler: ICommandHandler) {
        if(this.commandHandlers.has(commandHandler.getCommand())) {
            console.error(`A command handler for the command "${commandHandler.getCommand()}" has already been registered!`);
            return;
        }
        this.commandHandlers.set(commandHandler.getCommand(), commandHandler);
    }
}