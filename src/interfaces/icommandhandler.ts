import {Message} from 'discord.js';
import { IModule } from './imodule';
export enum ICommandHandlerArgType {
    ANY,
    NUMBER,
    INTEGER,
    FLOAT
}

export interface ICommandHandler {
    getCommand(): string,
    getArgRestrictions(): ICommandHandlerArgRestriction[],
    handleCommand(data: ICommandHandlerData): void
    getHelp(): string
}

export interface ICommandHandlerArgRestriction {
    name: string
    position: number,
    argType: ICommandHandlerArgType
    applyRestriction(content: string): any
    getHelp(): string
}

export interface ICommandHandlerData {
    message: Message
    args: any[]
    requestedModules?: Map<string, IModule>
}