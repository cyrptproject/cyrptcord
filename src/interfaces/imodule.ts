import { Controller } from "../controller";
import { PersistentStorageHandler } from "../storage";

export interface IModuleInitData {
    controller: Controller,
    dependencies?: IModule[],
    optionalDependencies?: IModule[]
}

export interface IModulePreInitData {
    storage: PersistentStorageHandler
    absoluteModulePath: string | undefined
}

export interface IModule {
    identifier: string,
    name: string,
    author: string,
    dependencies: IModule[],
    optionalDependencies: IModule[],
    preInit(data: IModulePreInitData): void,
    init(data: IModuleInitData): void,
    postInit(): void
}