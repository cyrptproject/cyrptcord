import { Controller } from "../../controller";
import { IModule, IModuleInitData, IModulePreInitData } from "../../interfaces/imodule";
import { PersistentStorageHandler } from "../../storage";
import { ICurrencyConfig } from "./interfaces/config";

class Currency implements IModule {
    identifier: string = "currency";
    name: string = "Currency";
    author: string = "Ocnoc";
    dependencies: IModule[] = [];
    optionalDependencies: IModule[] = [];

    private currencyName: string = '';

    preInit(data: IModulePreInitData): void {
        
    }
    init(data: IModuleInitData): void {
    }
    postInit(): void {
    }

}
export = new Currency();