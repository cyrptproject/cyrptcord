import { Message } from "discord.js";
import { CommandHandler } from "../../../commandhandler";
import { CommandHandlerArgRestriction } from "../../../commandhandlerargrestriction";
import { ICommandHandlerArgRestriction, ICommandHandlerArgType, ICommandHandlerData } from "../../../interfaces/icommandhandler";

export class Ping extends CommandHandler {
    protected command: string = "ping";
    protected argRestrictions: ICommandHandlerArgRestriction[] = [new CommandHandlerArgRestriction("target", 0, ICommandHandlerArgType.ANY)];
    handleCommand(data: ICommandHandlerData): void {
        if(!data.message.mentions.members) {
            return;
        }
        data.message.channel.send(`${data.message.mentions.members.first()} pong!`);
    }

}