import { Model } from "sequelize/types";
import { IModule, IModuleInitData, IModulePreInitData } from "../../interfaces/imodule";
import { PersistentStorageHandler } from "../../storage";
import { Ping } from "./commands/ping";

class PingPong implements IModule {
    identifier: string = "pingpong";
    name: string = "PingPong";
    author: string = "Ocnoc";
    dependencies: IModule[] = [];
    optionalDependencies: IModule[] = [];
    preInit(data: IModulePreInitData): void {
    }
    init(data: IModuleInitData): void {
        data.controller.registerCommandHandler(new Ping());
    }
    postInit(): void {
        
    }

}

export = new PingPong();