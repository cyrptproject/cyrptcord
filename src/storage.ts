import { Dialect, InitOptions, Model, ModelAttributes, ModelCtor, Sequelize } from "sequelize";
import { IModule } from "./interfaces/imodule";

export interface IStorageConnectionData {
    host: string,
    driver: Dialect,
    port: number,
    username: string,
    password: string,
    database: string
}

export class PersistentStorageHandler {
    private dbConnection: Sequelize | null

    constructor() {
        this.dbConnection = null;
    }

    private convertModelName(modelName: string, module: IModule) {
        return `${module.identifier}_${modelName}`;
    }

    public initConnection({host, driver, port, username, password, database}: IStorageConnectionData): void {
        if(driver == 'sqlite') {
            this.dbConnection = new Sequelize({
                dialect: driver,
                storage: host
            });
            return;
        }
        this.dbConnection = new Sequelize(database, username, password, {
            dialect: driver,
            host: host,
            port: port
        });
    }

    public registerModel(modelCtor: ModelCtor<Model<ModelAttributes, InitOptions>>, modelOptions: ModelAttributes, registeringModule: IModule): Model {
        if(this.dbConnection == null) {
            throw new Error("Attempting to register a model before database has been initialized.");
        }
        const validModelName: string = this.convertModelName(modelCtor.name, registeringModule);
        const initOptions: InitOptions = {
            sequelize: this.dbConnection,
            modelName: validModelName,
            freezeTableName: true
        }
        const model: Model = modelCtor.init(modelOptions, initOptions);
        return model;
    }
}